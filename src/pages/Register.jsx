import React, { useCallback, useState, useEffect } from "react";
import {HOST} from "../host";
import RegisterForm from "../components/RegisterForm";

const Register = () => {
    
	const handleCreateUser = async (form) => {
		console.log(form);
		try {
			await fetch(`${HOST}/api/users`, {
				method: 'post',
				headers: {'Content-Type':'application/json'},
				body: JSON.stringify(form)
			})
			.then(response => response.json())
			.then(data => console.log(data));;
		} catch (error) {
			console.error(error);
		}
	};

    return  (
        <div className="layout">
            <div className="leftCol pl-16 pr-16">
                <RegisterForm type="student" submitForm={handleCreateUser}/>
            </div>
            <div className="rightCol pl-16 pr-16">
				<RegisterForm type="teacher" submitForm={handleCreateUser}/>
            </div>
        </div>
    )
}

export default Register;