import Layout from "../utils/Layout";
import React, { useState, useMemo } from "react";
import ApproveTable from "../components/ApproveTable";

const AdminApproval = () => {
	const data = useMemo(
		() => [
		  { name: "Marius Georgescu"},
		  { name: "Alina Popa"},
		  { name: "Alexandra Matei"},
		  { name: "Sorin Popescu"},
		],	
		[]
	  );
	return  (
		<Layout>
			<div className="title">
				<h1> View Join Requests for Teachers </h1>
			</div> 
			<div >
				<ApproveTable data={data} noHref/>
			</div>
		</Layout>
	)
}

export default AdminApproval;