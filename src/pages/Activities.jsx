import Layout from "../utils/Layout";
import React, { useCallback, useState, useEffect } from "react";
import ActivityCard from "../components/ActivityCard";
import Button from "../components/Button";
import {MdAdd } from "react-icons/md";
import AddActivity from "../components/AddActivity"
import { useAuth0 } from "@auth0/auth0-react";
import {HOST} from "../host";

const Activities = () => {
	const { user } = useAuth0();
	const [availableActivity, setAvailableActivity] = useState([]);
	const [addActivity, setAddActivity] = useState(false);
	const [userGrade, setGrade] = useState(0);
	// -------- Get role of user ----------
	const [userRole, setUserRole] = useState(false);
	function getIsTeacher(data) {
		const {isTeacher} = data; 
		if (isTeacher === false) {
			const {grade} = data;
			setGrade(grade);
		}
		setUserRole(isTeacher);
	} 
	try {
		fetch(`${HOST}/api/users/${user.sub}`)
			.then(response => response.json())
			.then(data => getIsTeacher(...data));
		// console.log(fetch(`${HOST}/api/users/${user.sub}`));
	} catch(error) {
		console.error(error);
	}
	// -------------------------------------

	const getAllActivities = useCallback(async () => {
		try {
			console.log(userRole);
			if (userRole === false) {
				await fetch(`${HOST}/api/activities/student/${userGrade}`)
				.then(response => response.json())
				.then((data) => { setAvailableActivity(data, {userRole});});
			} else {
				await fetch(`${HOST}/api/activities/teacher/${user.sub}`)
				.then(response => response.json())
				.then((data) => { setAvailableActivity(data, {userRole}); });
			}
		} catch(error) {
			console.log(error);
		}
		
	}, [userGrade, userRole, user.sub]);

	const handleCreateActivity = async (form) => {
		try {
			await fetch(`${HOST}/api/activities`, {
				method: 'post',
				headers: {'Content-Type':'application/json'},
				body: JSON.stringify(form)
			})
			.then(response => response.json())
			.then(data => console.log(data));;
		} catch (error) {
			console.error(error);
		}
	};
	useEffect(() => {
		// console.log("Ne plac baietii\n");
		getAllActivities();
	}, [getAllActivities]);
	// console.log(user[authSettings.rolesKey])
	// console.log("AICI NU NE MAI PLAC BAIETII\n");
	return  (
		<Layout>
			 <AddActivity
				modalIsOpen={addActivity}
				closeModal={() => {
				setAddActivity(false);
				}}
				submitForm={handleCreateActivity}
			/>  
			<div className="title">
				{userRole === false ? 
					<h3> Hi, student! </h3> :
					<h3> Hi, teacher! </h3> 
				}
			</div> 
			<div>
				{userRole === false ? 
					<p> Here are some activities you might like:  </p> :
					<div className="row-between"> 
						<p> Here are your current activities:  </p>
						<Button onClick={() => setAddActivity(true)}>
						<MdAdd /> New Activity </Button>
					</div> 
				}
				
			</div> 
			<div className="activities"> { 
			availableActivity.map((activity, index) => (
			<ActivityCard key={index} {...activity} role={userRole}/>
			))} 
			</div>
		</Layout>
	)
}

export default Activities;