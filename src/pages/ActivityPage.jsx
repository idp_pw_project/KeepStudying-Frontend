import React, { useState, useMemo, useCallback, useEffect } from "react";
import { useLocation, useNavigate } from "react-router-dom";
import Button from "../components/Button";
import Layout from "../utils/Layout";
import { MdEdit, MdDelete, MdFileDownload } from "react-icons/md";
import AddActivity from "../components/AddActivity";
import Table from "../components/Table";
import title_icon from "../icons/education.png";
import teacher_icon from "../icons/teacher.png";
import grade_icon from "../icons/grade.png";
import date_icon from "../icons/calendar.png";
import location_icon from "../icons/location.png";
import link_icon from "../icons/link.png";
import Section from "../components/Section";
import {HOST} from "../host";
import EditActivity from "../components/EditActivity";

const ActivityPage = () => {
  const navigate = useNavigate();
  const [openedModal, setOpenedModal] = useState(false);
  const [activityInfo, setData] = useState({}); 
  const activityFields = useMemo(
    () =>[
    { icon: title_icon, key: "Title", value: activityInfo.title },
    { icon: teacher_icon, key: "Teacher", value: activityInfo.teacher },
    { icon: grade_icon, key: "Grade", value: activityInfo.grade },
    { icon: date_icon, key: "Date", value: activityInfo.date },
    { icon: date_icon, key: "Time", value: activityInfo.time },
    { icon: link_icon, key: "Link", value: activityInfo.link},
  ], [activityInfo]
  );
  const { pathname } = useLocation();
  const title = pathname.split("/").reverse()[0];
  const getActivity = useCallback( async () => {
    try {
      await fetch(`${HOST}/api/activities/${title}`)
			.then(response => response.json())
			.then(data => setData(data));
    } catch (error) {
      console.log(error);
    }
  }
  )
  useEffect(() => {
    getActivity();
  }, [getActivity]);

  const columns = [
    {
      Header: "Name",
      accessor: "name",
    },
    {
      Header: "Grade",
      accessor: "grade",
    },
  ];

  const data = useMemo(
    () => [
      {
        name: "Andrei Gheorghe",
        grade: "9",
      },
      {
        name: "Sorina Ionescu",
        grade: "10",
      },
    ],
    []
  );
  const handleEditActivity = async (form) => {
		try {
			await fetch(`${HOST}/api/activities/${activityInfo.title}`, {
				method: 'put',
				headers: {'Content-Type':'application/json'},
				body: JSON.stringify(form)
			})
			.then(response => response.json())
			.then(data => console.log(data));;
		} catch (error) {
			console.error(error);
		}
	};
  const handleDelete = async () => {
    alert("Are you sure you want to delete this activity?");
    try {
			await fetch(`${HOST}/api/activities/${activityInfo.title}`, {
				method: 'delete',
			})
			.then(response => response.json())
			.then(data => console.log(data));;
		} catch (error) {
			console.error(error);
		}
    navigate(`/activities`)
  };
  return (
    <Layout>
      <EditActivity
        modalIsOpen={openedModal}
        closeModal={() => {
          setOpenedModal(false);
        }}
        submitForm={handleEditActivity}
        title={title}
      />
      <div className="row-between">
        <h2>{title}</h2>
        <div className="row-center">
          <Button onClick={() => setOpenedModal(true)}>
            <MdEdit /> Edit
          </Button>
          <Button
            className="delete-button"
            onClick={handleDelete}
          >
            <MdDelete /> Delete
          </Button>
        </div>
      </div>
      <div className="flex flex-col gap-10">
        <Section title={"Class Details"} fields={activityFields} />
        <div className="flex flex-col gap-5 w-full p-[1px]">
          <p className="section-title">Support material</p>
          <Button >
            <MdFileDownload />Download Support
          </Button>
        </div>
        <div className="flex flex-col gap-5 w-full p-[1px]">
          <p className="section-title">Students</p>
          <Table data={data} columns={columns} noHref/>
        </div>
      </div>
    </Layout>
  );
};

export default ActivityPage;
