import Layout from "../utils/Layout";
import React, {useCallback, useState, useEffect} from "react";
import profile_icon from "../icons/profile_icon.png"
import Button from "../components/Button";
import { MdEdit, MdFileDownload } from "react-icons/md";
import teacher_icon from "../icons/teacher.png";
import email_icon from "../icons/email.png"
import telephone_icon from "../icons/telephone.png";
import subject_icon from "../icons/education.png";
import Section from "../components/Section";
import { useAuth0 } from "@auth0/auth0-react";
import { authSettings } from "../AuthSettings";
import { useNavigate } from "react-router-dom";
import {HOST} from "../host";
import EditProfile from "../components/EditProfile";

const Profile = () => {
	const [openedModal, setOpenedModal] = useState(false);
	const {user } = useAuth0();
	const navigate = useNavigate();
	// -------- Get role of user ----------
	const [userRole, setUserRole] = useState(false);
	function getIsTeacher(data) {
		const {isTeacher} = data; 
		setUserRole(isTeacher);
	} 
	try {
		fetch(`${HOST}/api/users/${user.sub}`)
			.then(response => response.json())
			.then(data => getIsTeacher(...data));
	} catch(error) {
		console.error(error);
	}
	// -------------------------------------

	const [activityInfo, setData] = useState({}); 
	const keyStr = userRole === false ? "Grade" : "Subject" ;
	const valueStr = userRole === false ? activityInfo.grade : activityInfo.subject ;
	const details = [
		{ icon: teacher_icon, key: "Name", value: activityInfo.name },
		{ icon: email_icon, key: "Email", value: activityInfo.email },
		{ icon: telephone_icon, key: "Phone", value: activityInfo.phone },
		{ icon: subject_icon, key: keyStr, value: valueStr },
	  ];

	const getProfile = useCallback( async () => {
	try {
		await fetch(`${HOST}/api/users/${user.sub}`)
			.then(response => response.json())
			.then(data => setData(...data));
	} catch (error) {
		console.log(error);
	}
	});
	useEffect(() => {
		getProfile();
	}, [getProfile]);
	const handleEditProfile = async (form) => {
		try {
			await fetch(`${HOST}/api/users/${user.sub}`, {
				method: 'put',
				headers: {'Content-Type':'application/json'},
				body: JSON.stringify(form)
			})
			.then(response => response.json())
			.then(data => console.log(data));;
		} catch (error) {
			console.error(error);
		}
	};
	return  (
		<Layout>
			<EditProfile
				modalIsOpen={openedModal}
				closeModal={() => {
				setOpenedModal(false)}}
				fields={details}
				submitForm={handleEditProfile}
			/>
			<div className="row-between">
				<h2 className="capitalize"> {user.nickname} </h2>
				<div className="row-center">
				{user[authSettings.rolesKey] == 'KeepStudying-admin' ? 
					<div className="row-center">
						<Button onClick={() => navigate(`/profile/admin_approve`)}> View Requests </Button>
					</div> : null}				
				<Button  onClick={() => setOpenedModal(true)}> <MdEdit /> Edit </Button>
				</div>
			</div>
			<div className="main">
				<div className="leftCol">
					<div className="icon-picture"> <img  src={profile_icon} alt="error"  width="400" height="400"/> </div>
					<div className="flex flex-col pl-20">
						<Section title={"Details"} fields={details} />
					</div> 
				</div>
				<div className="rightCol">
					{userRole === false ? null :
						<div className="flex flex-col pl-20">
							<h3> Resume/CV </h3>
							<div className="cv"> 
								<div className="row-center">
									<p className="flex flex-col pl-6">CV.pdf</p>
									<span className="flex pl-48 py-6"> <Button>
										<MdFileDownload /> Download CV
									</Button> </span>
								</div>
							</div>
						</div>
					}
					<div className="flex flex-col pl-20 py-16">
						<h3> Activity History </h3> 
						<div className="education"> </div>
					</div>
				</div>
				
			</div>	
		</Layout>
	)
}
export default Profile;