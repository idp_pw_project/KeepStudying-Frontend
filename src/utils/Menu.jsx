import logo from "../logo.png";
import read from "../icons/read.png";
import user from "../icons/user.png";
import menu_picture from "../icons/menu-picture.png"
import React from "react";
import { Link, useLocation, useMatch, useResolvedPath } from "react-router-dom";

export function CustomLink({ children, to, ...props }) {
	const resolved = useResolvedPath(to);
	const location = useLocation();
	const match =
	  useMatch({ path: resolved.pathname, end: true }) ??
	  location.pathname.includes(to);
  
	return (
	  <div>
		<Link
		  to={to}
		  {...props}
		  className={"nav-link" + (match ? " active" : "")}
		>
		  {children}
		</Link>
	  </div>
	);
  }

const Menu = () => {
	return (
	  <div className="admin-menu">
		<div className="logo-container">
		  <img src={logo} alt="KeepStudying" />
		</div>
		<nav className="admin-nav">
		  <CustomLink to={"/profile"}>
		    <img src={user} alt="err" width="20" height="20" /> Profile
		  </CustomLink>
		  <CustomLink to={"/activities"}>
		    <img src={read} alt="err" width="20" height="20" /> Activities
		  </CustomLink>
		</nav>
		<div className="picture-menu">
		  <img src={menu_picture} alt="KeepStudying" />
		</div>
	  </div>
	);
  };
  
  export default Menu;