import React from "react";
// import { useNavigate } from "react-router-dom";
import Menu from "./Menu";
import Header from "./Header";

const Layout = ({ children }) => {
	return (
	  <div className="layout">
		<Menu />
		<div className="content">
		  <Header />
		  {children}
		</div>
	  </div>
	);
  };
  
  export default Layout;