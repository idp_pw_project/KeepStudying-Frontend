import { BrowserRouter, Route, Routes } from "react-router-dom";
import Profile from "../pages/Profile";
import Activities from "../pages/Activities";
import AdminApproval from "../pages/AdminApproval";
import ActivityPage from "../pages/ActivityPage";
import React, { useCallback, useState, useEffect } from "react";
import { useAuth0 } from "@auth0/auth0-react";
import Register from "../pages/Register";
import {HOST} from "../host";

const Router = () => {
	const { user, isAuthenticated, loginWithRedirect } = useAuth0();
	const [isRegister, setRegister] = useState(false);
	useEffect(() => {
		if (!isAuthenticated) {
			loginWithRedirect();
		}
	}, [isAuthenticated, loginWithRedirect]);
	
	function isRegisterOrLogin() {
		try {
			fetch(`${HOST}/api/users/${user.sub}`)
			.then(response => response.json())
			.then(data => data.length === 0 ? setRegister(true) : setRegister(false));
		} catch (error) {
			console.log(error);
		}
		return isRegister == true ? <div> <Register /> </div> : <div>  <Activities/></div>;
	};

	return (
		isAuthenticated && (<BrowserRouter>
		  <Routes>
		  	<Route path="/" element={isRegisterOrLogin()} />
			<Route exact path="/profile" element={<Profile />} />
			<Route exact path="/activities" element={<Activities/>} />
			<Route exact path="/activities/activity_page/:title" element={<ActivityPage />} />
			<Route exact path="/profile/admin_approve" element={<AdminApproval />} />
			
		  </Routes>
		</BrowserRouter>)
	  );
  };
  
  export default Router;