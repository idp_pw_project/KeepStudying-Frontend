import http from "../api/config";  

const headers = (token) => ({  
    headers: {  
        Authorization: `Bearer ${token}`,  
    },  
});  

class ActivitiesDataService {  
    getAll() {  
        return http.get("/activities");  
    }  

    get(id) {  
        return http.get(`/activities/${id}`);  
    }  

    create(data) {  
        return http.post("/activities", data, headers(data.get("token")));  
    }  

    update({ id, data }) {  
        return http.put(`/activities/${id}`, data, headers(data.get("token")));  
    }  

    delete({ id, token }) {  
        return http.delete(`/activities/${id}`, headers(token));  
    }  

    deleteAll(token) {  
        return http.delete("/activities", headers(token));  
    }  

    findByTitle(title) {  
        return http.get(`/activities?title=${title}`);  
    }  

    like(data) {  
        return http.patch(`/activities/${data.id}/likePost`, {}, headers(data.token));  
    }  
}  

export default new ActivitiesDataService();