import axios from "axios";

export default axios.create({
  baseURL: "${HOST}/api",
  headers: {
    "Content-type": "application/json"
  }
});