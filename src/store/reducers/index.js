import { combineReducers } from "redux";
import activities from "./activityReducer";


export default combineReducers({
    activities,
});