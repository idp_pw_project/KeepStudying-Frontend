import {  
    CREATE_ACTIVITY,
    GET_ACTIVITIES,  
	RETRIEVE_ACTIVITIES,  
	UPDATE_ACTIVITY,  
	DELETE_ACTIVITY,  
	DELETE_ALL_ACTIVITIES,
	LIKE_ACTIVITY,  
} from "../actions/types";  

const initialState = [];  

function activityReducer(activities = initialState, action) {  
    const { type, payload } = action;  

    switch (type) {  
        case CREATE_ACTIVITY:  
            return [...activities, payload];  
        case GET_ACTIVITIES:
            return payload;
            
        case RETRIEVE_ACTIVITIES:  
            return payload;  

        case UPDATE_ACTIVITY:  
            return activities.map((post) => {  
                if (post.id === payload.id) {  
                    return {  
                    ...post,  
                    ...payload,  
                    };  
                }  
                return post;  
            });  

        case DELETE_ACTIVITY:  
            return activities.filter(({ id }) => id !== payload.id);  

        case DELETE_ALL_ACTIVITIES:  
            return [];  

        case LIKE_ACTIVITY:  
            return activities.map((post) =>  
                post.id === action.payload.id ? action.payload : post  
            );  

        default:  
            return activities;  
    }  
}  

export default activityReducer;
