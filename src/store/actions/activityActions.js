import {  
	CREATE_ACTIVITY,
	GET_ACTIVITIES,  
	RETRIEVE_ACTIVITIES,  
	UPDATE_ACTIVITY,  
	DELETE_ACTIVITY,  
	DELETE_ALL_ACTIVITIES, 
	LIKE_ACTIVITY,  
  } from './types';  
  import ActivitiesDataService from '../../services/activitiesServices'
  
  export const createActivity =  (formData) => async (dispatch) => {
	try {
		const res = await ActivitiesDataService.create(formData);
	
		dispatch({
		  type: CREATE_ACTIVITY,
		  payload: res.data,
		});
	
		return Promise.resolve(res.data);
	  } catch (err) {
		return Promise.reject(err);
	  }
  };  
  
  export const findAllActivities = () => async (dispatch) => {
	try {
		const res = await ActivitiesDataService.getAll();
	
		dispatch({
		  type: GET_ACTIVITIES,
		  payload: res.data,
		});
	
		return Promise.resolve(res.data);
	  } catch (err) {
		return Promise.reject(err);
	  }
  };

  export const retrieveActivities = () => async (dispatch) => {};  
  
  export const updateActivity = ({id, data}) => async (dispatch) => {};  
  
  export const deleteActivity = ({id, token}) => async (dispatch) => {};  
  
  export const deleteAllActivities = (token) => async (dispatch) => {};  
  
  export const findActivitiesByTitle = (title) => async (dispatch) => {};  
  
  export const likeActivity = (data) => async (dispatch) => {};