import React, { useState } from "react";
import Button from "./Button";
import { useNavigate } from "react-router-dom";
import { MdEdit, MdDelete, MdFileDownload } from "react-icons/md";
import subject_icon from "../icons/whiteboard.png";
import title_icon from "../icons/education.png";
import teacher_icon from "../icons/teacher.png";
import date_icon from "../icons/calendar.png";
import time_icon from "../icons/clock.png";
import {HOST} from "../host";
import { useAuth0 } from "@auth0/auth0-react";

const ActivityCard = ({ subject, title, teacher, date, time, isFull, role, link, handleClick }) => {
  const navigate = useNavigate();
  const { user } = useAuth0();
  const userRole = role;
  const [joinActivity, setJoinActivity] = useState(false);
  const handleDelete = async () => {
    alert("Are you sure you want to delete this activity?");
    try {
			await fetch(`${HOST}/api/activities/${title}`, {
				method: 'delete',
			})
			.then(response => response.json())
			.then(data => console.log(data));;
		} catch (error) {
			console.error(error);
		}
  };

  const handleJoin = async () => {
    const body = {"activity": title, "link" : link, "email" : user.email};
    console.log(body)
    try {
			await fetch(`${HOST}/api/email`, {
				method: 'post',
				headers: {'Content-Type':'application/json'},
				body: JSON.stringify(body)
			})
			.then(response => response.json())
			.then(data => console.log(data));;
		} catch (error) {
			console.error(error);
		}
    setJoinActivity(true);
  }

  const handleActivityPageClick = (title) => {
    navigate(`/activities/activity_page/${title}`)
  };

  return (
    <div className={`activity-card`}>
      <div className="activity-group">
        <div className="card-heading">
            <img src={subject_icon} alt="subject" width="20" height="20" /> &nbsp; <p>{subject}</p>
        </div>
        <div className="card-content">
            <img src={title_icon} alt="subject" width="20" height="20" /> &nbsp; <h3 className="title">{title}</h3>
        </div>
        <div className="card-heading">
            <img src={teacher_icon} alt="subject" width="20" height="20" /> &nbsp; <p>{teacher}</p>
        </div>
        <div className="card-content">
            <img src={date_icon} alt="subject" width="20" height="20" /> &nbsp; <p>{date}</p> 
        </div>
        <div className="card-content">
            <img src={time_icon} alt="subject" width="20" height="20" /> &nbsp; <p>{time}</p> 
        </div>
      </div>
      <div className="row-center">
        {userRole === false ? 
            <div className="row-center">
              {!isFull ? 
                <div className="row-center">
                  {!joinActivity && <Button className={"upload-button"} onClick={() => {handleJoin()}} > Join </Button>}
                  {joinActivity && <Button className={"delete-button"} onClick={() => {setJoinActivity(false)}}> Joined </Button>}
                  <Button> <MdFileDownload /> Download </Button>
                </div> :
                <div> 
                  <Button disabled> Class is full </Button>
                </div> 
            }
            </div> : 
            <div className="row-center"> 
              <Button onClick={() => handleActivityPageClick(title)}>View </Button>
              <Button className="delete-button"  onClick={handleDelete}> <MdDelete /> Delete</Button>
            </ div>}	
        </div>
    </div>
  );
};

export default ActivityCard;
