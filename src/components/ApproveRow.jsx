import React from "react";
import user from "../icons/user.png";
import Button from "./Button";
import { MdFileDownload } from "react-icons/md";

const ApproveRow = ({name}) => {
	console.log(name);
	return (
		<div className="approve-row">
			<div className="approve-row-left">
				<img src={user} alt="SetProfile" width="20" height="20" /> &nbsp;
				<p className="text-lg"> {name} </p>
			</div>
			<div className="approve-row-center"> <Button > <MdFileDownload /> CV </Button></div>
			<div className="approve-row-right">
				<Button className="green-button">  Approve </Button>
				<p>&nbsp; &nbsp;</p>
				<Button className="delete-button"> Deny </Button>
			</div>
		</div>
	);
};
  
  export default ApproveRow;