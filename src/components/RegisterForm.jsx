import React from "react";
import user from "../icons/user.png";
import Button from "./Button";
import Input from "./Input";
import logo from "../logo.png";
import icon_student from "../icons/register_student.png";
import icon_teacher from "../icons/register_teacher.png";
import { useNavigate } from "react-router-dom";
import { useForm } from "react-hook-form";
import { useAuth0 } from "@auth0/auth0-react";

const RegisterForm = ({type, submitForm}) => {
	const navigate = useNavigate();
	const { register, handleSubmit, getValues } = useForm();
	const { user } = useAuth0();
	const handleClick = async () => {
		const data = getValues();
		const userId = user.sub;
		const isTeacher = type == "student" ? false : true;
		submitForm({ ...data, userId, isTeacher});
		navigate(`/activities`);
	  };
	return (
		<div>
			<div className="flex flex-row justify-center py-6">
				<div className="logo-container">
					<img src={logo} alt="KeepStudying" />
				</div>
			
			</div>
			<div className="flex flex-row justify-center"> <h2>Welcome, {type}!</h2> </div>
			<div className="flex flex-row" />
				<form>
					<div> <Input label="Name" placeholder="Insert here" {...register("name")} />  </div>
					<div>  <Input label="Email"  placeholder="Insert here" {...register("email")}/>  </div>
					<div>  <Input label="Phone"  placeholder="Insert here" {...register("phone")}/> </div>
					{type == "student" ? 
					<div> <Input label="Grade" placeholder="Insert here" {...register("grade")}/> </div> : 
					<div> <Input label="Subject" placeholder="Insert here" {...register("subject")}/> </div>
					}
				</form>
			<div className="flex flex-row justify-center py-4">
				<Button onClick={handleSubmit(handleClick)}>Register as {type}</Button>
			</div>
			{type =="student" ? 
			<div className="register-picture "> <img src={icon_student} alt="error" width="200" height="200" /></div> : 
			<div className="register-picture py-16"> <img src={icon_teacher} alt="error" width="200" height="200" /></div> }
		</div>
	);
};
  
  export default RegisterForm;