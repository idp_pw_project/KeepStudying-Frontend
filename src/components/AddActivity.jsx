import React, {useState} from "react";
import Modal from "react-modal";
import { MdOutlineClose } from "react-icons/md";
import Button from "./Button";
import Input from "./Input";
import title_icon from "../icons/education.png";
import teacher_icon from "../icons/teacher.png";
import grade_icon from "../icons/grade.png";
import date_icon from "../icons/calendar.png";
import location_icon from "../icons/location.png";
import link_icon from "../icons/link.png";
import students_icon from "../icons/students.png";
import {MdAdd } from "react-icons/md";
import { useAuth0 } from "@auth0/auth0-react";
import { useForm } from "react-hook-form";

const AddActivity = ({ modalIsOpen, closeModal, submitForm }) => {
  const { register, handleSubmit, getValues } = useForm();
  const { logout, user } = useAuth0();
  
  const handleClick = async () => {
    const data = getValues();
    const userId = user.sub;
    submitForm({ ...data, userId });
    closeModal();
  };

  return (
    <Modal
      isOpen={modalIsOpen}
      onRequestClose={closeModal}
      contentLabel="Add activity"
      className="modal"
      
    >
      <div className="row-between">
       <h2>Activity</h2>
        <Button onClick={closeModal} className="icon-button">
          <MdOutlineClose />
        </Button>
      </div>
      <div className="line" />
      <form>
        <Input label="Title"  icon={teacher_icon} placeholder="Insert here" {...register("title")}/>
        <Input label="Subject" icon={title_icon} placeholder="Insert here" {...register("subject")}/>
        <Input label="Grade" icon={grade_icon} placeholder="Insert here" {...register("grade")}/>
        <Input label="Date" icon={date_icon} placeholder="Insert here" {...register("date")}/>
        <Input label="Time" icon={date_icon} placeholder="Insert here" {...register("time")}/>
        <Input label="Link" icon={link_icon} placeholder="Insert here" {...register("link")}/>
        <Input label="Max no studs" icon={students_icon} placeholder="Insert here" {...register("maxNoStud")}/>
        <div className="row-center">
            <Button className="upload-button" > <MdAdd />  Upload Material </Button>
            <Button type="button" onClick={handleSubmit(handleClick)}> Create </Button>
        </div>
        </form>
    </Modal>
  );
};

export default AddActivity;
