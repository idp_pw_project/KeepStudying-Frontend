import React from "react";
import Modal from "react-modal";
import { MdOutlineClose } from "react-icons/md";
import Button from "./Button";
import Input from "./Input";
import { useForm } from "react-hook-form";
import { useAuth0 } from "@auth0/auth0-react";

const EditProfile = ({ modalIsOpen, closeModal, fields, submitForm }) => {
    const { register, handleSubmit, getValues } = useForm();
    const { logout, user } = useAuth0();
    const handleClick = async () => {
        const data = getValues();
        const userId = user.sub;
        console.log(data)
        submitForm({ ...data, userId });
        closeModal();
    };
  return (
    <Modal
      isOpen={modalIsOpen}
      onRequestClose={closeModal}
      contentLabel="Edit account"
      className="modal"
    >
      <div className="row-between">
        <h2>Edit account</h2>
        <Button onClick={closeModal} className="icon-button">
          <MdOutlineClose />
        </Button>
      </div>
      <div className="line" />
      <form>
        <Input label={fields[0].key} placeholder="Insert here" {...register(fields[0].key)}/>
        <Input label={fields[1].key} placeholder="Insert here"  {...register(fields[1].key)}/>
        <Input label={fields[2].key} placeholder="Insert here"  {...register(fields[2].key)}/>
        <Input label={fields[3].key} placeholder="Insert here"  {...register(fields[3].key)}/>
        <Button type="button" onClick={handleSubmit(handleClick)}>
          Save
        </Button>
      </form>
    </Modal>
  );
};

export default EditProfile;
