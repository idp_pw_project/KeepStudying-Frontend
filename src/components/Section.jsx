import React from "react";
const Section = ({ title, fields }) => {
  return (
    <div className="section-container">
      <p className="section-title">{title}</p>
      <div className="fields">
        {fields &&
          fields.map((field) => (
            <>
            <div className="row-left"> 
                <img src={field.icon} alt="error" width="25" height="20" /> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                 {field.key && <span>{field.key}</span>} 
            </div>
            <span className="section-title">{field.value}</span>
            </>
          ))} 
      </div>
    </div>
  );
};

export default Section;
