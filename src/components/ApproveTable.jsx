import React, {useMemo, useState } from "react";
import ApproveRow from "./ApproveRow";


const ApproveTable = ({data, noHref }) => {
	 // Use the state and functions returned from useTable to build your UI
	return (
	<table class="bg-white">
		<thead>
		<div className="approve-row">
			<div className="approve-row-left"> <h1>Name</h1> </div>
			<div className="approve-row-center"> <h1>CV</h1> </div>
			<div className="approve-row-right"> <h1>Action</h1> </div>
		</div>
		<hr></hr>
		</thead>
		<tbody>
			{data.map((teacher, index) => (
				<ApproveRow name={teacher.name}> </ApproveRow>
			))}
    
        </tbody>
	</table>
	);
};
  
  export default ApproveTable;