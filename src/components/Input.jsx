import React, { forwardRef } from "react";

const Input = forwardRef((props, ref) => {
  const { label, icon, ...rest } = props;
  return (
    <label>
        <div className="row-left">  
            {icon ? <div> <img src={icon} alt="error" width="20" height="20" /> &nbsp; </div> : null}
            {label && <span>{label}</span>} 
        </div>
        <input {...rest} className={`input`} {...rest} ref={ref} />
    </label>
  );
});

export default Input;
