import React, {useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import { useTable } from "react-table";


const Table = ({data, columns, noHref }) => {
    const navigate = useNavigate();
  
    // Use the state and functions returned from useTable to build your UI
    const { getTableProps, getTableBodyProps, headerGroups, rows, prepareRow } =
      useTable({
        columns,
        data,
      });
  
    const [hoveredRow, setHoveredRow] = useState(null)
    // Render the UI for your table
    return (
      <table class="bg-white" {...getTableProps()} 
      >
        <thead>
          {headerGroups.map((headerGroup) => (
            <tr {...headerGroup.getHeaderGroupProps()}>
              {headerGroup.headers.map((column) => (
                <th {...column.getHeaderProps()}>{column.render("Header")}</th>
              ))}
            </tr>
          ))}
        </thead>
        <tbody {...getTableBodyProps()}>
          {rows.map((row, i) => {
            prepareRow(row);
            return (
              <tr class="hover:bg-blue-accent "
                {...row.getRowProps()}
                className="cursor-pointer bg-gray-50 hover:bg-blue-accent "
                // onClick={() => handleRowClick(i)}
              >
                {row.cells.map((cell) => {
                  return <td  {...cell.getCellProps()}>{ cell.render("Cell")}</td>;
                })}
              </tr>
            );
          })}
        </tbody>
      </table>
    );
  };
  
  export default Table;