module.exports = {
    content: ["./src/**/*.{jsx,js}"],
    theme: {
        fontFamily: {
            sans: ["Quicksand", "Quicksand"],
        },
        boxShadow: {
            DEFAULT: "0px 1px 2px rgba(0, 0, 0, 0.03)",
        },
        screens: {
            md: "768px",
            lg: "1024px",
            xl: "1280px",
        },
        colors: {
            transparent: "transparent",
            current: "currentColor",
            white: "#FEFEFE",
            black: "#111111",
            mainbackground: "#F1F8FF",
            blue: {
                faded: "#16324F0d",
                accent: "#FFBF4A",
                secondary: "#16324F",
                DEFAULT: "#C2F2FF",
            },
            gray: {
                DEFAULT: "#C0C0C0",
                background: "#11111138",
            },
            red: {
                DEFAULT: "#F45858",
            },
            green: {
                DEFAULT: "#51A154",
            },
        },
    },
    plugins: [],
};